/*
  
  Copyright (c) 1997-2021 Victor Lavrenko (v.lavrenko@gmail.com)
  
  This file is part of YARI.
  
  YARI is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  YARI is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the GNU General Public License
  along with YARI. If not, see <http://www.gnu.org/licenses/>.
  
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "vector.h"
#include "hash.h"
#include "textutil.h"

int *col_names (char **cols, int n, char *hdr) {
  hdr += strspn(hdr," #"); // skip initial
  hdr = strdup(hdr);
  char **F = split(hdr,'\t');
  int *nums = calloc(n,sizeof(int)), i, j, NF = len(F);
  for (i=0; i<n; ++i) {
    if (cols[i][0] == '\\') continue; // skip \literal
    for (j=0; j<NF; ++j)
      if (!strcmp(cols[i],F[j])) nums[i] = j+1;
    if (!nums[i]) {
      fprintf(stderr,"ERROR: no field '%s' in: '%s'\n", cols[i], hdr);
      exit(1);
    }
  }
  free_vec(F);
  free(hdr);
  return nums;
}

int *col_nums (char **cols, int n, char *hdr) {
  int i; char *s;
  for (i=0; i<n; ++i) 
    if (cols[i][0] != '\\') // skip \literal
      for (s=cols[i]; *s; ++s) 
	if (!isdigit(*s)) return col_names (cols, n, hdr);
  int *nums = calloc(n,sizeof(int));
  for (i=0; i<n; ++i) nums[i] = atoi(cols[i]);
  return nums;
}

void cut_tsv (char *line, int *nums, int n, char **cols) {
  char **F = split(line,'\t');
  int i, NF = len(F);
  for (i = 0; i < n; ++i) {
    int literal = (cols[i][0] == '\\'), c = nums[i];
    char *val = literal ? (cols[i]+1) : (c > 0 && c <= NF) ? F[c-1] : "";
    char sep = (i < n-1) ? '\t' : '\n';
    fputs (val,stdout);
    fputc (sep,stdout);
  }
  free_vec(F);
}

void cut_json (char *line, char **cols, int n) {
  int i;
  for (i = 0; i < n; ++i) {
    char *val = json_value (line, cols[i]);
    char sep = (i < n-1) ? '\t' : '\n';
    if (val) { fputs(val,stdout); free (val); }
    fputc (sep, stdout);
  }
}
 
int json_like (char *line) { return line[strspn(line," ")] == '{'; }
 
void cut_stdin (char **cols, int n) {
  size_t sz = 999999;
  int *nums = NULL, nb = 0;
  char *line = malloc(sz), type = 0;
  while (0 < (nb = getline(&line,&sz,stdin))) {
    if (line[nb-1] == '\n') line[nb-1] = '\0';
    if (!type) {
      type = json_like (line) ? 'J' : 'T';
      if (type == 'T') nums = col_nums (cols, n, line);
    }
    if (type == 'J') cut_json(line, cols, n);
    if (type == 'T') cut_tsv (line, nums, n, cols);
  }
}

int strip_xml () {
  size_t sz = 999999;
  int nb = 0;
  char *line = malloc(sz);
  while (0 < (nb = getline(&line,&sz,stdin))) {
    csub (line, "\t\r", ' ') ; // tabs and CR -> space
    noxml(line); // erase <...>
    noquot(line); // erase &#x14e;
    spaces2space (line); // multiple -> single space
    chop (line, " "); // remove leading / trailing space
    fputs (line, stdout);
  }
  return 0;
}

int wc_stdin () {
  size_t sz = 999999;
  unsigned long NB = 0, NW = 0, NL = 0;
  int nb = 0;
  char *line = malloc(sz);
  while (0 < (nb = getline(&line,&sz,stdin))) {
    NL += 1; // number of lines
    NB += nb; // number of bytes
    int ws = 1; char *s;    
    for (s = line; *s; ++s) {
      if (*s == ' ') ws = 1;
      else if (ws) { ws = 0; ++NW; } // space -> word transition
    }
    //erase_between (line, "<", ">", ' '); // remove all tags
    //fputs (line, stdout);
  }
  printf ("\t%ld\t%ld\t%ld\t lines/words/bytes\n", NL, NW, NB);
  return 0;
}

void show_header (char **cols, int n) {
  int i; fputc ('#', stdout);
  for (i = 0; i < n; ++i) {
    char sep = (i < n-1) ? '\t' : '\n';
    fputs (cols[i], stdout); 
    fputc (sep, stdout);
  }
}

/*
void stdin_grams (char *prm) {
  uint k = getprm(prm,"gram=",2);
  size_t sz = 999999, nb = 0, i;
  char *line = malloc(sz);
  while (0 < (nb = getline(&line,&sz,stdin))) {
    if (line[nb-1] == '\n') line[nb-1] = '\0';
    for (i=0; i<nb-k; ++i) ...
  }  
}
*/

char *usage = 
  "xcut 3 2 \\X 17    ... print columns 3, 2, 'X', 17 from stdin\n"
  "xcut age size type ... use 1st line to map age -> column number\n"
  "xcut age size type ... stdin = {JSON} records one-per-line\n"
  ;

int main (int argc, char *argv[]) {
  if (argc < 2) return fprintf (stderr, "\n%s\n", usage);
  if (!strcmp(argv[1],"-h")) { ++argv; --argc; show_header(argv+1,argc-1); }
  if (!strcmp(argv[1],"-noxml")) return strip_xml();
  if (!strcmp(argv[1],"-wc")) return wc_stdin();
  cut_stdin(argv+1,argc-1);
  return 0;
}
